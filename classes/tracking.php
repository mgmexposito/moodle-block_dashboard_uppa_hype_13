<?php

namespace block_dashboard_uppa_hype_13;

use block_dashboard_uppa_hype_13\repository\moodle_repository;
use block_dashboard_uppa_hype_13\repository\elk_repository;
use block_dashboard_uppa_hype_13\elkfollowup;
use block_dashboard_uppa_hype_13\dashboard_data;
use Exception;
use context_course;
use stdClass;

class tracking
{
    public static function get_tracking_for_course($course_moodle_id)
    {
        global $USER;

        $learner_moodle_id = $USER->id;
        $moodle_repository = new moodle_repository();
        $elk_repository = new elk_repository(
            get_config('block_dashboard_uppa_hype_13', 'host'),
            get_config('block_dashboard_uppa_hype_13', 'user'),
            get_config('block_dashboard_uppa_hype_13', 'password'),
            get_config('block_dashboard_uppa_hype_13', 'elk_dataprefix'),
            get_config('block_dashboard_uppa_hype_13', 'traxactprefix'),
        );
        $elkfollowup = new elkfollowup();
        $dashboard_data = new dashboard_data();

        $course_context = context_course::instance($course_moodle_id);
        $is_manager = false;
        if (has_capability('moodle/course:ignoreavailabilityrestrictions', $course_context)) {
            $is_manager = true;
        }
        try {
            $course_structure = $moodle_repository->get_course_m_structure($course_moodle_id);

            $course_enrolments = $moodle_repository->get_course_m_registrations($course_moodle_id);

            $course_trax_external_id = $moodle_repository->get_course_m_external_id($course_moodle_id);
        } catch (Exception $e) {
            throw new Exception("Invalid request to db");
        }

        if (($course_structure === false) || ($course_enrolments === false)) {
            return "no data for this course";
        }

        // preparation de la structure du cours pour ELK
        $moodle_course_structure = $elk_repository->get_elk_moodle_structure($course_structure, $course_trax_external_id);

        //recuperation des donnees elk
        $res_elk = $elk_repository->get_data_elk($moodle_course_structure);

        $res_elk_followUp = $elkfollowup->create_elk_user_followUp($res_elk);

        $elk_data = $elkfollowup->get_simple_dashboard_data($res_elk_followUp, (object)array("moodle_trax_ids" => "-1"));

        $assoc_id_moodle_trax = (object)array(
            "MtoX" => (object)array(),
            "XtoM" => (object)array(),
            "XtoFLName" => (object)array()
        );
        for ($i = 0; $i < count($course_enrolments); $i++) {
            $assoc_id_moodle_trax->MtoX->{$course_enrolments[$i][23]} = $course_enrolments[$i][26];
            $assoc_id_moodle_trax->XtoM->{$course_enrolments[$i][26]} = $course_enrolments[$i][23];
            $assoc_id_moodle_trax->XtoFLName->{$course_enrolments[$i][26]} = (object)array(
                "email" => $course_enrolments[$i][0],
                "firstname" => $course_enrolments[$i][1],
                "lastname" => $course_enrolments[$i][2]
            );
        };

        $learners_follow_up = $elkfollowup->get_elk_learners_follow_up($course_enrolments, $elk_data);

        /**
         * DASHBOARDS APPRENANT
         */
        $descriptive_informations = $dashboard_data->get_dashboard_descriptive_infos($learners_follow_up, "bestUserRes");
        $model_to_compare = $descriptive_informations;

        /**
         * DASHBOARDS ENSEIGNANT 
         */
        $descriptive_informations_for_teachers = (object)array();
        $diagnostic_informations_for_teachers = (object)array();
        $predictive_informations_for_teachers = (object)array();
        //
        for ($i = 0; $i < count($course_enrolments); $i++) {
            $tLearner = $course_enrolments[$i][26];
            $descriptive_informations_for_teachers->{$tLearner} = $dashboard_data->get_dashboard_descriptive_infos($learners_follow_up, $tLearner);
            $diagnostic_informations_for_teachers->{$tLearner} = $dashboard_data->get_dashboard_diagnostic_infos($descriptive_informations_for_teachers->{$tLearner});
            $predictive_informations_for_teachers->{$tLearner} = $dashboard_data->get_dashboard_predictive_infos($descriptive_informations_for_teachers->{$tLearner}, $model_to_compare);
        }

        /**
         * Organisation du tableau par progression
         */
        $output_dash_multi = array();
        foreach ($descriptive_informations_for_teachers as $learner => $tracking) {

            if ($assoc_id_moodle_trax->XtoM->{$learner} == $learner_moodle_id) {
                $tOutputLearner = "Mon suivi";
            } else if (!$is_manager) {
                $tOutputLearner = $learner;
            } else {
                $tOutputLearner = $assoc_id_moodle_trax->XtoFLName->{$learner}->firstname . " " . $assoc_id_moodle_trax->XtoFLName->{$learner}->lastname;
            }

            array_push($output_dash_multi, array(
                "learner" => $tOutputLearner,
                "completion_ress" => round(floatval($descriptive_informations_for_teachers->{$learner}->me->completed_ress) * 100, 0),
                "participation_ques" => round(floatval($descriptive_informations_for_teachers->{$learner}->me->participation_ques) * 100, 0),
                "performance_ques" => round(floatval($descriptive_informations_for_teachers->{$learner}->me->performance) * 100, 0),
                "completion_diag" => $diagnostic_informations_for_teachers->{$learner}->completed_ress,
                "participation_diag" => $diagnostic_informations_for_teachers->{$learner}->participation_ques,
                "performance_diag" => $diagnostic_informations_for_teachers->{$learner}->performance,
            ));
        }

        $results = new stdClass();
        $results->learners_results = $output_dash_multi;
        $results->course_results = $descriptive_informations;
        return $results;
    }
}
