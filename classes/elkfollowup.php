<?php

namespace block_dashboard_uppa_hype_13;

use Exception;

class elkfollowup
{
    public function get_simple_dashboard_data($elk_user_followUp, $moodle_trax_ids)
    {
        /* sortie 

    */
        $simple_dashboard_data = (object)array();
        //best
        if (!isset($simple_dashboard_data->bestUserRes)) {
            $simple_dashboard_data->bestUserRes = self::return_empty_simple_dashboard_data();
        }
        $simple_dashboard_data->bestUserRes->volumes = $elk_user_followUp->bestUserRes->follow_up->volumes;
        $simple_dashboard_data->bestUserRes->ratios = self::get_user_simple_dashboard_ratios($simple_dashboard_data->bestUserRes->volumes, $simple_dashboard_data->bestUserRes->volumes);
        //
        foreach ($elk_user_followUp as $k => $v) {
            if ($k != "bestUserRes") {
                if (!isset($simple_dashboard_data->{$k})) {
                    $simple_dashboard_data->{$k} = self::return_empty_simple_dashboard_data();
                }
                $simple_dashboard_data->{$k}->volumes = $elk_user_followUp->{$k}->follow_up->volumes;
                $simple_dashboard_data->{$k}->ratios = self::get_user_simple_dashboard_ratios($simple_dashboard_data->{$k}->volumes, $simple_dashboard_data->bestUserRes->volumes);
            }
        }
        //
        return $simple_dashboard_data;
    }

    public function create_elk_user_followUp($res_elk_output)
    {
        //echo"create_elk_user_followUp >> \r\n";
        //print_r($res_elk_output);
        //
        $raw_followUP = (object)array();
        $res_followUP = (object)array();
        //
        if (isset($res_elk_output->progression)) {
            try {
                $raw_followUP->progression = json_decode($res_elk_output->progression);
                //
                //Get Max
                if (!isset($res_followUP->bestUserRes)) {
                    $res_followUP->bestUserRes = self::return_empty_followUp_object();
                }
                $res_followUP->bestUserRes->follow_up->volumes->completed_ress = 0;
                for ($i = 0; $i < count($raw_followUP->progression->aggregations->apprenant->buckets); $i++) {
                    if ($res_followUP->bestUserRes->follow_up->volumes->completed_ress < $raw_followUP->progression->aggregations->apprenant->buckets[$i]->act_count->value) {
                        $res_followUP->bestUserRes->follow_up->volumes->completed_ress = $raw_followUP->progression->aggregations->apprenant->buckets[$i]->act_count->value;
                    }
                }
                //
                for ($i = 0; $i < count($raw_followUP->progression->aggregations->apprenant->buckets); $i++) {
                    $learner_uuid = $raw_followUP->progression->aggregations->apprenant->buckets[$i]->key;
                    //
                    if (!isset($res_followUP->{$learner_uuid})) {
                        $res_followUP->{$learner_uuid} = self::return_empty_followUp_object();
                    }
                    //
                    $res_followUP->{$learner_uuid}->follow_up->max_to_complete = $res_followUP->bestUserRes->follow_up->volumes->completed_ress;
                    //
                    //
                    $res_followUP->{$learner_uuid}->user_data->trax_id = $learner_uuid;
                    $res_followUP->{$learner_uuid}->user_data->moodle_id = -1; //toDo
                    $res_followUP->{$learner_uuid}->elk_data->progression = $res_elk_output->progression;
                    //
                    $res_followUP->{$learner_uuid}->follow_up->volumes->completed_ress = $raw_followUP->progression->aggregations->apprenant->buckets[$i]->act_count->value;
                }
            } catch (Exception $e) {
                echo "create_elk_user_followUp/progression, error:" . $e;
            }
        } else {
            echo "res_elk_output->progression not set";
        }
        //
        if (isset($res_elk_output->participation)) {
            try {
                $raw_followUP->participation = json_decode($res_elk_output->participation);
                //
                //Get Max
                if (!isset($res_followUP->bestUserRes)) {
                    $res_followUP->bestUserRes = self::return_empty_followUp_object();
                }
                $res_followUP->bestUserRes->follow_up->volumes->participation_ress = 0;
                for ($i = 0; $i < count($raw_followUP->participation->aggregations->apprenant->buckets); $i++) {
                    if ($res_followUP->bestUserRes->follow_up->volumes->participation_ress < $raw_followUP->participation->aggregations->apprenant->buckets[$i]->act_count->value) {
                        $res_followUP->bestUserRes->follow_up->volumes->participation_ress = $raw_followUP->participation->aggregations->apprenant->buckets[$i]->act_count->value;
                    }
                }
                //
                for ($i = 0; $i < count($raw_followUP->participation->aggregations->apprenant->buckets); $i++) {
                    $learner_uuid = $raw_followUP->participation->aggregations->apprenant->buckets[$i]->key;
                    //
                    if (!isset($res_followUP->{$learner_uuid})) {
                        $res_followUP->{$learner_uuid} = self::return_empty_followUp_object();
                    }
                    //
                    $res_followUP->{$learner_uuid}->user_data->trax_id = $learner_uuid;
                    $res_followUP->{$learner_uuid}->user_data->moodle_id = -1; //toDo
                    $res_followUP->{$learner_uuid}->elk_data->participation = $res_elk_output->participation;
                    //
                    $res_followUP->{$learner_uuid}->follow_up->volumes->participation_ress = $raw_followUP->participation->aggregations->apprenant->buckets[$i]->act_count->value;
                }
            } catch (Exception $e) {
                echo "create_elk_user_followUp/participation, error:" . $e;
            }
        } else {
            echo "res_elk_output->participation not set";
        }
        //
        if (isset($res_elk_output->questions)) {
            try {
                $raw_followUP->questions = json_decode($res_elk_output->questions);
                //
                //Get Max
                if (!isset($res_followUP->bestUserRes)) {
                    $res_followUP->bestUserRes = self::return_empty_followUp_object();
                }
                $res_followUP->bestUserRes->follow_up->volumes->performance = 0;
                for ($i = 0; $i < count($raw_followUP->questions->aggregations->apprenant->buckets); $i++) {
                    //
                    if ($res_followUP->bestUserRes->follow_up->volumes->participation_ques < count($raw_followUP->questions->aggregations->apprenant->buckets[$i]->ress->buckets)) {
                        $res_followUP->bestUserRes->follow_up->volumes->participation_ques = count($raw_followUP->questions->aggregations->apprenant->buckets[$i]->ress->buckets);
                    }
                    //
                    if ($res_followUP->bestUserRes->follow_up->volumes->performance < $raw_followUP->questions->aggregations->apprenant->buckets[$i]->q_score->value) {
                        $res_followUP->bestUserRes->follow_up->volumes->performance = $raw_followUP->questions->aggregations->apprenant->buckets[$i]->q_score->value;
                    }
                }
                //
                for ($i = 0; $i < count($raw_followUP->questions->aggregations->apprenant->buckets); $i++) {
                    $learner_uuid = $raw_followUP->questions->aggregations->apprenant->buckets[$i]->key;
                    //
                    if (!isset($res_followUP->{$learner_uuid})) {
                        $res_followUP->{$learner_uuid} = self::return_empty_followUp_object();
                    }
                    //
                    $res_followUP->{$learner_uuid}->user_data->trax_id = $learner_uuid;
                    $res_followUP->{$learner_uuid}->user_data->moodle_id = -1; //toDo
                    $res_followUP->{$learner_uuid}->elk_data->questions = $res_elk_output->questions;
                    //
                    $res_followUP->{$learner_uuid}->follow_up->listes->participation_ques = $raw_followUP->questions->aggregations->apprenant->buckets[$i]->ress;
                    $res_followUP->{$learner_uuid}->follow_up->volumes->participation_ques = count($raw_followUP->questions->aggregations->apprenant->buckets[$i]->ress->buckets);
                    $res_followUP->{$learner_uuid}->follow_up->volumes->performance = $raw_followUP->questions->aggregations->apprenant->buckets[$i]->q_score->value;
                }
            } catch (Exception $e) {
                echo "create_elk_user_followUp/questions, error:" . $e;
            }
        } else {
            echo "res_elk_output->questions";
        }

        return $res_followUP;
    }

    public function get_user_simple_dashboard_ratios($me, $best)
    {
        $simple_dashboard_ratios_res = (object)array();
        foreach ($best as $k => $v) {
            // echo "get_user_simple_dashboard_ratios > k:".$k.">> v:".$v." \r\n";
            if (!is_nan($v) && ($v >= 0)) {
                // echo "me->k:".$me->{$k}." >> v:".$me->{$k}." == ".$me->{$k}."/".$best->{$k}." >>> ".($me->{$k}/$best->{$k})." \r\n";
                if (!is_nan($me->{$k}) && ($me->{$k} >= 0)) {
                    $simple_dashboard_ratios_res->{$k} = number_format($me->{$k} / $best->{$k}, 2);
                } else {
                    $simple_dashboard_ratios_res->{$k} = number_format(0, 2);
                }
            } else {
                $simple_dashboard_ratios_res->{$k} = number_format(0, 2);
            }
        }
        // print_r($simple_dashboard_ratios_res);
        return $simple_dashboard_ratios_res;
    }

    public function return_empty_simple_dashboard_data()
    {
        return (object)array(
            "user_data" => (object)array(
                "trax_id" => -1,
                "moodle_id" => -1
            ),
            "volumes" => (object)array(
                "completed_ress" => -1,
                "participation_ress" => -1,
                "participation_ques" => -1,
                "performance" => -1,
                "success_ress" => -1
            ),
            "ratios" => (object)array(
                "completed_ress" => -1,
                "participation_ress" => -1,
                "participation_ques" => -1,
                "performance" => -1,
                "success_ress" => -1
            )
        );
    }

    public function return_empty_followUp_object()
    {
        return (object)array(
            "user_data" => (object)array(
                "trax_id" => -1,
                "moodle_id" => -1
            ),
            "elk_data" => (object)array(
                "progression" => (object)array(),
                "participation" => (object)array(),
                "question" => (object)array()
            ),
            "follow_up" => (object)array(
                "max_to_complete" => -1,
                "listes" => (object)array(
                    "completed_ress" => array(),
                    "participation_ress" => array(),
                    "participation_ques" => array(),
                    "performance" => array(),
                    "success_ress" => array()
                ),
                "volumes" => (object)array(
                    "completed_ress" => -1,
                    "participation_ress" => -1,
                    "participation_ques" => -1,
                    "performance" => -1,
                    "success_ress" => -1
                ),
                "ratios" => (object)array(
                    "completed_ress" => -1,
                    "participation_ress" => -1,
                    "participation_ques" => -1,
                    "performance" => -1,
                    "success_ress" => -1
                ),
                "ress_details" => (object)array(),
                "q_details" => (object)array()
            )
        );
    }

    /**
* SUIVI APPRENANT (ELK)
*/
public function get_elk_learners_follow_up($course_enrolments,$elk_data) {
    $learners_follow_up=(object)array();
    $traxid_to_moodleid=(object)array();
    for($i=0;$i<count($course_enrolments);$i++){
        if ( ($course_enrolments[$i][26]!="") && ($course_enrolments[$i][23]!="") ) {
            $learners_follow_up->{$course_enrolments[$i][26]} = self::init_learner_data($course_enrolments[$i][23],$course_enrolments[$i][26]);
            $traxid_to_moodleid->{$course_enrolments[$i][26]}=$course_enrolments[$i][23];
        }
    }

    foreach($elk_data as $k=>$v) {

        $acteur = $k;
        (isset($traxid_to_moodleid->{$acteur})) ? $mid=$acteur : $mid=-1;
        if (!isset($learners_follow_up->{$acteur})) $learners_follow_up->{$acteur} = self::init_learner_data($mid,$acteur);

        $learners_follow_up->{$acteur}->follow_up->volumes->completed_ress = $v->volumes->completed_ress;
        $learners_follow_up->{$acteur}->follow_up->volumes->participation_ress = $v->volumes->participation_ress;
        $learners_follow_up->{$acteur}->follow_up->volumes->participation_ques = $v->volumes->participation_ques;
        $learners_follow_up->{$acteur}->follow_up->volumes->performance = $v->volumes->performance;
        $learners_follow_up->{$acteur}->follow_up->volumes->success_ress = $v->volumes->success_ress;


        $learners_follow_up->{$acteur}->follow_up->ratios->completed_ress = $v->ratios->completed_ress;
        $learners_follow_up->{$acteur}->follow_up->ratios->participation_ress = $v->ratios->participation_ress;
        $learners_follow_up->{$acteur}->follow_up->ratios->participation_ques = $v->ratios->participation_ques;
        $learners_follow_up->{$acteur}->follow_up->ratios->performance = $v->ratios->performance;
        $learners_follow_up->{$acteur}->follow_up->ratios->success_ress = $v->ratios->success_ress;
     

    }

    return $learners_follow_up;

}

//
private function init_learner_data($moodle_id, $trax_id) {
    $model=(object)array(
        "user_data"=>(object)array(
            "trax_id"=>$trax_id,
            "moodle_id"=>$moodle_id
        ),
        "follow_up"=>(object)array(
            "max_to_complete"=>0,
            "listes"=>(object)array(
                "completed_ress"=>array(),
                "participation_ress"=>array(),
                "participation_ques"=>array(),
                "performance"=>array(),
                "success_ress"=>array()
            ),
            "volumes"=>(object)array(
                "completed_ress"=>0,
                "participation_ress"=>0,
                "participation_ques"=>0,
                "performance"=>0,
                "success_ress"=>0
            ),
            "ratios"=>(object)array(
                "completed_ress"=>0,
                "participation_ress"=>0,
                "participation_ques"=>0,
                "performance"=>0,
                "success_ress"=>0
            ),
            "ress_details"=>(object)array(),
            "q_details"=>(object)array()
        )
    );
    return $model;
}
}
