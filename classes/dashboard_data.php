<?php

namespace block_dashboard_uppa_hype_13;

class dashboard_data
{
    public function get_dashboard_descriptive_infos($learners_follow_up, $learner_id)
    {
        $me = (object)array(
            "completed_ress" => $learners_follow_up->{$learner_id}->follow_up->ratios->completed_ress,
            "participation_ress" => $learners_follow_up->{$learner_id}->follow_up->ratios->participation_ress,
            "participation_ques" => $learners_follow_up->{$learner_id}->follow_up->ratios->participation_ques,
            "performance" => $learners_follow_up->{$learner_id}->follow_up->ratios->performance,
            "success_ress" => $learners_follow_up->{$learner_id}->follow_up->ratios->success_ress,
        );
        //class datas
        $class_data = (object)array(
            "all_inscriptions" => (object)array(
                "datas" => (object)array(),
                "ratios" => (object)array()
            ),
            "only_those_with_participations" => (object)array(
                "datas" => (object)array(),
                "ratios" => (object)array()
            )
        );
        //max
        $minmax = (object)array(
            "min" => (object)array(),
            "max" => (object)array()
        );
        //recuperation des données de la classe (learner_id inclus)
        foreach ($learners_follow_up as $learner => $l_data) {
            foreach ($me as $subject => $notused) {
                //
                if (!isset($class_data->all_inscriptions->datas->{$subject})) $class_data->all_inscriptions->datas->{$subject} = array();
                array_push($class_data->all_inscriptions->datas->{$subject}, $l_data->follow_up->ratios->{$subject});
                //
                if (!isset($class_data->only_those_with_participations->datas->{$subject})) $class_data->only_those_with_participations->datas->{$subject} = array();
                if ($l_data->follow_up->ratios->{$subject} > 0) {
                    array_push($class_data->only_those_with_participations->datas->{$subject}, $l_data->follow_up->ratios->{$subject});
                }
                //minmax -> min
                if (!isset($minmax->min->{$subject})) $minmax->min->{$subject} = $l_data->follow_up->ratios->{$subject};
                if ($minmax->min->{$subject} > $l_data->follow_up->ratios->{$subject}) $minmax->min->{$subject} = $l_data->follow_up->ratios->{$subject};
                //minmax -> max
                if (!isset($minmax->max->{$subject})) $minmax->max->{$subject} = $l_data->follow_up->ratios->{$subject};
                if ($minmax->max->{$subject} < $l_data->follow_up->ratios->{$subject}) $minmax->max->{$subject} = $l_data->follow_up->ratios->{$subject};
            }
        }
        //creation des ratios
        foreach ($me as $subject => $notused) {
            foreach ($class_data as $segment => $alsonotused) {
                if (!isset($class_data->{$segment}->ratios->{$subject})) $class_data->{$segment}->ratios->{$subject} = 0;
                $class_data->{$segment}->ratios->{$subject} = self::get_ratio_from_array($class_data->{$segment}->datas->{$subject});
            }
        }
        //
        return (object)array(
            "meid" => $learner_id,
            "me" => $me,
            "all_inscriptions" => $class_data->all_inscriptions->ratios,
            "only_those_with_participations" => $class_data->only_those_with_participations->ratios,
            "minmax" => $minmax
        );
    }

    private function get_ratio_from_array($number_arr)
    {
        if (count($number_arr) > 0) {
            $res = number_format(array_sum($number_arr) / count($number_arr), 2, '.', '');;
        } else {
            $res = 0;
        }
        return $res;
    }

    public function get_dashboard_diagnostic_infos($descriptive_data)
    {
        //
        $colors = (object)array(
            "high" => "green",
            "mid" => "yellow",
            "low" => "red",
            "none" => "grey"
        );
        //
        $res_color = (object)array(
            "completed_ress" => null,
            "participation_ress" => null,
            "participation_ques" => null,
            "performance" => null,
            "success_ress" => null
        );
        //
        foreach ($res_color as $subject => $notused) {
            //choix "only_those_with_participations"
            if (
                ($descriptive_data->me->{$subject} > $descriptive_data->only_those_with_participations->{$subject} * 1.2)
                ||  ($descriptive_data->me->{$subject} == $descriptive_data->minmax->max->{$subject})
            ) {
                $res_color->{$subject} = $colors->high;
            } else if (
                ($descriptive_data->me->{$subject} <= $descriptive_data->only_those_with_participations->{$subject} * 1.2)
                &&  ($descriptive_data->me->{$subject} > $descriptive_data->only_those_with_participations->{$subject} * 0.8)
            ) {
                $res_color->{$subject} = $colors->mid;
            } else if ($descriptive_data->me->{$subject} <= $descriptive_data->only_those_with_participations->{$subject} * 0.8) {
                $res_color->{$subject} = $colors->low;
            } else {
                $res_color->{$subject} = $colors->none;
            }
        }
        return $res_color;
    }

    public function get_dashboard_predictive_infos($desriptive_informations, $model_to_compare)
    {
        //to do
        return self::get_dashboard_diagnostic_infos($desriptive_informations);
    }
}
