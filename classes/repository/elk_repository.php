<?php

namespace block_dashboard_uppa_hype_13\repository;

const ELK_DEFAULT_HEADERS = array(
  "Content-Type"=>"application/json; charset=utf-8",
  "Accept"=>"*/*"
);

const ELK_INDEX = "statements";

$elk_prefs = (object)array(
  "address"=> getenv('ELASTIC_ADDRESS'),
  "basicAuth"=>(object)array(
      "username"=> getenv('ELASTIC_SEARCH_USERNAME'),
      "password"=> getenv('ELASTIC_SEARCH_PASSWORD')
  ),
  "dataprefix"=>"data.",
  "traxactprefix"=> getenv('TRAX_PREFIX')
);


class elk_repository
{
  private $elk_host;
  private $elk_username;
  private $elk_password;
  private $elk_dataprefix;
  private $elk_traxactprefix;

  public function __construct($elk_host, $elk_username, $elk_password, $elk_dataprefix, $elk_traxactprefix)
  {
    $this->elk_host = $elk_host;
    $this->elk_username = $elk_username;
    $this->elk_password = $elk_password;
    $this->elk_dataprefix = $elk_dataprefix;
    $this->elk_traxactprefix = $elk_traxactprefix;
  }

  public function get_elk_moodle_structure($m_course_structure, $m_uuid)
  {
    $res_cs = (object)array(
      "moodle_course_uuid" => $m_uuid,
      "moodle_activities" => array()
    );
    for ($i = 0; $i < count($m_course_structure); $i++) {
      array_push($res_cs->moodle_activities, $this->elk_traxactprefix . "/" . $m_course_structure[$i][12] . "/" . $m_course_structure[$i][17]);
    }
    return $res_cs;
  }
  /**
   * ELK DATA
   */
  public function get_data_elk($moodle_course_structure)
  {
    $elk_url = $this->elk_host . "/" . ELK_INDEX . "/_search";
    $auth = (object)array(
      "username" => $this->elk_username,
      "password" => $this->elk_password
    );
    $res = (object)array(
      "progression" => null,
      "participation" => null,
      "questions" => null
    );
    //Progression
    $reqPr = self::create_request_elk("progression", (object)array(
      "moodle_course_uuid" => $moodle_course_structure->moodle_course_uuid,
      "contextContextActivitiesGroupingIdKeyword" => $moodle_course_structure->moodle_activities
    ));

    $res->progression = self::php_xhr_elk("GET", ELK_DEFAULT_HEADERS, $elk_url, $auth, $reqPr);
    //Participatioon
    $reqPa = self::create_request_elk("participation", (object)array(
      "moodle_course_uuid" => $moodle_course_structure->moodle_course_uuid,
      "contextContextActivitiesGroupingIdKeyword" => $moodle_course_structure->moodle_activities
    ));

    $res->participation = self::php_xhr_elk("GET", ELK_DEFAULT_HEADERS, $elk_url, $auth, $reqPa);
    //Questions
    $reqQ = self::create_request_elk("questions", (object)array(
      "moodle_course_uuid" => $moodle_course_structure->moodle_course_uuid,
      "contextContextActivitiesGroupingIdKeyword" => $moodle_course_structure->moodle_activities
    ));

    $res->questions = self::php_xhr_elk("GET", ELK_DEFAULT_HEADERS, $elk_url, $auth, $reqQ);

    return $res;
  }

  /**
   * ELK REQUEST
   */
  private function create_request_elk($modOp, $params)
  {
    if ($modOp == "progression") {
      $request = '{
          "query": {
            "bool": {
              "must": [
                {
                  "match": {
                    "' . $this->elk_dataprefix . 'context.contextActivities.parent.definition.type.keyword" : "http://vocab.xapi.fr/activities/course" 
                  }
                },
                {
                  "match": {
                     "' . $this->elk_dataprefix . 'context.contextActivities.parent.id.keyword":"' . $this->elk_traxactprefix . '/course/' . $params->moodle_course_uuid . '" 
                  }
                },
                {
                  "terms": {
                    "' . $this->elk_dataprefix . 'verb.id.keyword": [
                        "http://adlnet.gov/expapi/verbs/completed", "http://vocab.xapi.fr/verbs/marked-completion", "http://adlnet.gov/expapi/verbs/passed", "http://adlnet.gov/expapi/verbs/scored", "http://vocab.xapi.fr/verbs/graded"
                    ]
                  }
                },
                {
                  "terms": {
                    "' . $this->elk_dataprefix . 'object.id.keyword": [
      ';
      for ($i = 0; $i < count($params->contextContextActivitiesGroupingIdKeyword); $i++) {
        if ($i > 0) $request .= ',';
        $request .= '"' . $params->contextContextActivitiesGroupingIdKeyword[$i] . '"';
      }
      $request .= ']
                  }
              }
              ]
            }
          },
          "size":0,
          "aggs": {
            "apprenant": {
                "terms": {
                    "field": "' . $this->elk_dataprefix . 'actor.account.name.keyword",
                    "size":1000
                },
                "aggs": {
                    "act_count": { 
                        "cardinality": { 
                            "field" : "' . $this->elk_dataprefix . 'object.id.keyword",
                            "precision_threshold": 100
                        }
                    },
                    "apps_bucket_sort": {
                      "bucket_sort": {
                        "sort": [
                          { "act_count.value": { "order": "desc" } } 
                        ]                               
                      }
                  }
                }
            }
          }
        }';
    } else if ($modOp == "participation") {
      $request = '{ 
          "query": {
            "bool": {
              "must": [
                {
                  "match": {
                    "' . $this->elk_dataprefix . 'context.contextActivities.parent.definition.type.keyword" : "http://vocab.xapi.fr/activities/course" 
                  }
                },
                {
                  "match": {
                    "' . $this->elk_dataprefix . 'context.contextActivities.parent.id.keyword":"' . $this->elk_traxactprefix . '/course/' . $params->moodle_course_uuid . '" 
                     }
                  },
                  {
                    "match": {
                      "' . $this->elk_dataprefix . 'verb.id.keyword": "http://vocab.xapi.fr/verbs/navigated-in"
                    }
                  },
                  {
                    "terms": {
                      "' . $this->elk_dataprefix . 'object.id.keyword": [
        ';
      for ($i = 0; $i < count($params->contextContextActivitiesGroupingIdKeyword); $i++) {
        if ($i > 0) $request .= ',';
        $request .= '"' . $params->contextContextActivitiesGroupingIdKeyword[$i] . '"';
      }
      $request .= ']
                    }
                }
              ]
            }
          },
          "size":0,
          "aggs": {
            "apprenant": {
                "terms": {
                    "field": "' . $this->elk_dataprefix . 'actor.account.name.keyword",
                    "size":1000
                },
                "aggs": {
                    "act_count": { 
                        "cardinality": { 
                            "field" : "' . $this->elk_dataprefix . 'object.id.keyword",
                            "precision_threshold": 100
                        }
                    },
                    "apps_bucket_sort": {
                      "bucket_sort": {
                        "sort": [
                          { "act_count.value": { "order": "desc" } } 
                        ]                               
                      }
                  }
                }
            }
          }
        }';
    } else if ($modOp == "questions") {
      // $moodle_course_uuid : "https://spoc-la.hype13.fr/xapi/activities/course/d16a0433-0f55-423b-948e-3e25fde165c8"
      $request = '{
            "query": {
              "bool": {
              "must": [
                  {
                    "match": {
                      "' . $this->elk_dataprefix . 'verb.id.keyword" : "http://adlnet.gov/expapi/verbs/answered" 
                    }
                  },
                  {
                    "match": {
                      "' . $this->elk_dataprefix . 'context.contextActivities.grouping.id.keyword":"' . $this->elk_traxactprefix . '/course/' . $params->moodle_course_uuid . '" 
                      }
                  },
                  {
                    "terms": {
                      "' . $this->elk_dataprefix . 'context.contextActivities.grouping.id.keyword": [
        ';
      for ($i = 0; $i < count($params->contextContextActivitiesGroupingIdKeyword); $i++) {
        if ($i > 0) $request .= ',';
        $request .= '"' . $params->contextContextActivitiesGroupingIdKeyword[$i] . '"';
      }
      $request .= ']
                    }
                }
             ]
            }
          },
          "size":0,
          "aggs": {
            "apprenant": {
                "terms": {
                    "field": "' . $this->elk_dataprefix . 'actor.account.name.keyword",
                    "size":1000
                },
                "aggs": {
                	"ress": { 
                        "terms": { 
                            "field" : "' . $this->elk_dataprefix . 'object.id.keyword",
                            "size":1000
                        },
                    	"aggs": {
                    		"max_score": { 
            					"max": { 
        							"field": "' . $this->elk_dataprefix . 'result.score.scaled" 
                				}
                			}
                		}
                	},
                	"q_score": {
                		"sum_bucket": {
                			"buckets_path": "ress>max_score"
                		}
                	},
                    "apps_bucket_sort": {
                      "bucket_sort": {
                        "sort": [
                          { "q_score.value": { "order": "desc" } } 
                        ]                               
                      }
                  }
                }
            }
          }
        }';
    } else {
      $request = false;
    }
    return $request;
  }


  /**
   * PHP XHR ELK
   */
  private function php_xhr_elk($method, $headers, $elk_url, $auth, $data)
  {

    if ($data != null) $method = "POST";

    $curlObj = curl_init();

    /**do not check SSL*/
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, false);

    if ($method == "POST") {
      curl_setopt($curlObj, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($curlObj, CURLOPT_POST, true);
      curl_setopt($curlObj, CURLOPT_POSTFIELDS, $data);
      array_push($headers->auto, "\"Content-Length: " . strlen($data) . "\"");
    } else {
      curl_setopt($curlObj, CURLOPT_CUSTOMREQUEST, "GET");
    }

    curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlObj, CURLOPT_VERBOSE, true);
    curl_setopt($curlObj, CURLOPT_HEADER, true);
    curl_setopt($curlObj, CURLOPT_USERPWD, $auth->username . ":" . $auth->password);
    curl_setopt($curlObj, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curlObj, CURLOPT_POSTREDIR, 3);

    $c_headers = array();
    foreach ($headers as $k => $v) {
      array_push($c_headers, $k . ":" . $v);
    }
    curl_setopt($curlObj, CURLOPT_HTTPHEADER, $c_headers);
    curl_setopt($curlObj, CURLOPT_URL, $elk_url);


    if (!$response = curl_exec($curlObj)) {
      trigger_error(curl_error($curlObj));
      $body = false;
    } else {
      $header_size = curl_getinfo($curlObj, CURLINFO_HEADER_SIZE);
      $header = substr($response, 0, $header_size);
      $body = substr($response, $header_size);
    }

    $json = $body;

    curl_close($curlObj);

    return $json;
  }
}
