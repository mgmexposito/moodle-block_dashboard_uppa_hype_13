import ajax from 'core/ajax';
import modal from 'core/modal_factory';
import ModalEvents from 'core/modal_events';
import $ from 'jquery';

const DEFAULT_DESC_VALUE = true;

let course_results = null;
let learners_results = null;
let modalWindow = null;
let selectedSort = 'progression';
let desc = DEFAULT_DESC_VALUE;

const sortResults = (a, b) => {
    if (selectedSort === 'participation') {
        return b.participation_ques - a.participation_ques;
    }
    if (selectedSort === 'performance') {
        return b.performance_ques - a.performance_ques;
    }
    return b.completion_ress - a.completion_ress;
};

const isSortedBy = (name) => selectedSort === name;

const showSortArrowFor = (name) => {
    if (!isSortedBy(name)) {
        return '';
    }
    if (desc) {
        return `<i class="fa fa-solid fa-arrow-down"></i>`;
    }
    return `<i class="fa fa-solid fa-arrow-up"></i>`;
};

const renderResults = () => {
    let rows = learners_results.sort(sortResults);

    if (!desc) {
        rows = rows.reverse();
    }
    const htmlRows = rows.map(learner_results => {

        return `
        <tr>
        <td>${learner_results.learner}</td>
        <td class="dashboard_uppa_hype_13_diag_${learner_results.completion_diag}">${learner_results.completion_ress}%</td>
        <td class="dashboard_uppa_hype_13_diag_${learner_results.participation_diag}">${learner_results.participation_ques}%</td>
        <td class="dashboard_uppa_hype_13_diag_${learner_results.performance_diag}">${learner_results.performance_ques}%</td>
        </tr>
        `;
    }).join('');

    const renderHtml = `
    <table class="table table-bordered table-sm table-striped">
        <thead class="thead-light">
            <tr>
                <th></th>
                <th><a href="#" id="sort-by-progression">Progression ${showSortArrowFor('progression')}</a></th>
                <th><a href="#" id="sort-by-participation">Participation ${showSortArrowFor('participation')}</a></th>
                <th><a href="#" id="sort-by-performance">Performance ${showSortArrowFor('performance')}</a></th>
            </tr>
            <tr>
                <th>Moyenne de la classe</th>
                <th>${course_results.only_those_with_participations.completed_ress * 100}%</th>
                <th>${course_results.only_those_with_participations.participation_ques * 100}%</th>
                <th>${course_results.only_those_with_participations.performance * 100}%</th>
            </tr>
            <tr>
                <th>Minimum de la classe</th>
                <th>${course_results.minmax.min.completed_ress * 100}%</th>
                <th>${course_results.minmax.min.participation_ques * 100}%</th>
                <th>${course_results.minmax.min.performance * 100}%</th>
            </tr>
            <tr>
                <th>Maximum de la classe</th>
                <th>${course_results.minmax.max.completed_ress * 100}%</th>
                <th>${course_results.minmax.max.participation_ques * 100}%</th>
                <th>${course_results.minmax.max.performance * 100}%</th>
            </tr>
        </thead>
        <tbody>
            ${htmlRows}
        </tbody>
    </table>
    `;
    modalWindow.body.html(renderHtml);
    $("#sort-by-progression").on('click', (e) => {
        e.preventDefault();
        changeSort('progression');
    });
    $("#sort-by-performance").on('click', (e) => {
        e.preventDefault();
        changeSort('performance');
    });
    $("#sort-by-participation").on('click', (e) => {
        e.preventDefault();
        changeSort('participation');
    });
};

const disableRefreshButton = () => {
    $("#uppa-refresh-btn").prop("disabled", true);
};

const enableRefreshButton = () => {
    $("#uppa-refresh-btn").prop("disabled", false);
};

const changeSort = (sortValue) => {
    if (selectedSort === sortValue) {
        desc = !desc;
    } else {
        selectedSort = sortValue;
        desc = DEFAULT_DESC_VALUE;
    }
    renderResults();
};

const createGatewayhandler = (course_id) => () => {
    disableRefreshButton();
    var promises = ajax.call([
        { methodname: 'block_dashboard_uppa_hype_13_get_tracking', args: { course_id } }
    ]
    );

    promises[0].done(function (response) {
        const parsedResponse = JSON.parse(response.data);
        course_results = parsedResponse.course_results;
        learners_results = parsedResponse.learners_results;
        renderResults();
        enableRefreshButton();
    }).fail(function (error) {
        modalWindow.body.html(error.message);
        enableRefreshButton();
    });
};

export const init = (course_id) => {
    const trigger = $("#uppa-btn");

    modal.create({
        title: 'Suivi des activités du cours',
        body: '<div class="spinner-border"></div>',
        footer: '<button class="btn btn-primary" id="uppa-refresh-btn">Actualiser</button>',
        large: true,
    }, trigger)
        .done((modal) => {
            modalWindow = modal;
            const getTracking = createGatewayhandler(course_id);

            modalWindow.getRoot().on(ModalEvents.shown, () => {
                $("#uppa-refresh-btn").on('click', getTracking);
                getTracking();
            });
            modalWindow.getRoot().on(ModalEvents.hidden, () => {
                $("#uppa-refresh-btn").off('click');
            });
        });
};
