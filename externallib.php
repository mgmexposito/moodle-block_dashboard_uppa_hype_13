<?php

global $CFG;

require_once("$CFG->libdir/externallib.php");

use block_dashboard_uppa_hype_13\tracking;

class block_dashboard_uppa_hype_13_external extends external_api
{
    public static function get_tracking_parameters()
    {
        return new external_function_parameters (
            array(
                    'course_id' => new external_value(PARAM_INT, 'id of course')
            )
        );
    }

    public static function get_tracking_returns()
    {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW, "json ouput")
            )
        );
    }

    public static function get_tracking($course_id)
    { 
        self::validate_parameters(self::get_tracking_parameters(), array('course_id' => $course_id));
        $output = tracking::get_tracking_for_course($course_id);
        $data = new stdClass();
        $data->data = json_encode($output);
        return $data;

        
    }
}
