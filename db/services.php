<?php

$functions = array(
    'block_dashboard_uppa_hype_13_get_tracking' => array(         //web service function name
        'classname'   => 'block_dashboard_uppa_hype_13_external',  //class containing the external function OR namespaced class in classes/external/XXXX.php
        'methodname'  => 'get_tracking',          //external function name
        'description' => 'Get tracking from ELK',    //human readable description of the web service function
        'type'        => 'read',                  //database rights of the web service function (read, write)
        'ajax' => true,        // is the service available to 'internal' ajax calls. 
        'classpath' => 'blocks/dashboard_uppa_hype_13/externallib.php',
        'capabilities' => '', // comma separated list of capabilities used by the function.
    ),
);